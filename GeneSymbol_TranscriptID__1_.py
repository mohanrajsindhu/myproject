
import urllib.request as request
import json
import pandas as pd
from sqlalchemy import create_engine

from flask import Flask, render_template

engine = create_engine('sqlite://', echo=False)

with request.urlopen('https://d1hf8ajhua2dre.cloudfront.net/Altsplice/NCBI/CanonicalID/LongestCDS/CanonicalLIst.json') as response:
  source = response.read()
 # print(source)
  
  json_data = json.loads(source)
#   print(json_data) 
# f=open("samp.json",'w')
# json.dump(json_data,f)
# f.close()

df=pd.DataFrame(list(json_data.items()))
# print(df)
df.to_sql('Canonical_Transcript',con=engine, if_exists='replace')
gene_trans=engine.execute("SELECT * FROM Canonical_Transcript").fetchall()
# print(gene_trans)


file = open(r"C:\Users\91776\Desktop\genome\batch2\batch2programs\templates\index.html", "w")  # append mode
htmlstart=""" <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gene Symbol & Tanscript ID</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>
<body>
<center> <h1> Batch 2 Project </h1> </center>
<div class="display" >
<label id="genelabel" for="genesymbol">Gene Symbol</label>
<br>
<input list="gene" placeholder="Select Gene" id="genee" onchange="geneselect();">\n"""
file.write(htmlstart)
file.close()


def insertdf():
    file = open(r"C:\Users\91776\Desktop\genome\batch2\batch2programs\templates\index.html", "a")  # append mode
    file.write("<datalist id = 'gene'>\n")
    for i in range(len(gene_trans)):
        eachdf = gene_trans[i]
        genelist = "<option>"+eachdf[1]+"</option>\n"
        file.writelines(genelist)
    endlist =  "</datalist>\n" 
    file.writelines(endlist)
    file.writelines("</div>")
   
    file.writelines("""<div class="displayy">
<label id="translabel" for="TranscriptId">Transcript ID</label>
<br>
<input type='text' id='display'>
</div>\n""")

    dlist2="<datalist id = 'trans'>\n"
    file.writelines(dlist2)

    for i in range(len(gene_trans)):
        eachtrans = gene_trans[i]
        translist = "<option id='"+eachtrans[1]+"'>"+eachtrans[2]+"</option>\n"
        file.writelines(translist)
    file.writelines(endlist) 
    file.close()

    
insertdf()


endhtml="""<footer> Project done by Aravindh, Madhan and Sindhuja </footer>
</body>
<script>
function geneselect(){
var gene=document.getElementById("genee").value;
var trans=document.getElementById("trans").options.namedItem(gene).value;
if (trans!="")
document.getElementById('display').value = trans;
else
document.getElementById('display').value ="No Transcript ID Found";
}
</script>
<style>
   
    body{
     background-image:url('https://wallpapercave.com/wp/wp2170024.jpg');
     background-repeat:no-repeat;
     background-attachment:fixed;
     background-size:100% 100%;
    }
    .display{
        margin: auto;
        width: 30%;
        background: rgba(0,0,0,0.3);
  border: 2px solid #fff;
  border-radius: 10px;
  font-size: 18px;
        padding: 10px;
        text-align: center;
color: #fff;
font-family: Georgia, serif;
  outline: none;
        margin-top: 200px;
  
    }
    .displayy{
        margin: auto;
        width: 30%;
        background: rgba(0,0,0,0.3);
  border: 2px solid #fff;
  border-radius: 10px;
  font-size: 18px;
        padding: 10px;
font-family: Georgia, serif;
        text-align: center;
color: #fff;
  outline: none;
        margin-bottom: 200px;
    }
    #trans,#g{

        font-weight: bolder;
    }
    h1{
font-family: Georgia, serif;
        color: #fff;
    }
footer{font-family: Georgia, serif;
color: #fff;
}
</style>

</html> """
file = open(r"C:\Users\91776\Desktop\genome\batch2\batch2programs\templates\index.html", "a")  # append mode
file.write(endhtml)
file.close()




app = Flask(__name__)
@app.route('/')
def home():
    return render_template('index.html')
if __name__ == '__main__':
    app.run()
  

print("working.....")










